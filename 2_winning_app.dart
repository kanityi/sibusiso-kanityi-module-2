class WinningApp {
  String name;
  String award;
  int year;

  WinningApp(this.name, this.award, this.year);
}

void main(List<String> args) {
  var winningApps = <WinningApp>[
    WinningApp('FNB Banking', 'the best iOS consumer App', 2012),
    WinningApp('Health ID', 'the best iOS enterprise App', 2012),
    WinningApp('FNB Banking', 'the best Blackberry App', 2012),
    WinningApp(
        'TransUnion Dealer Guide', 'the best Android enterprise App', 2012),
    WinningApp('FNB Banking', 'the best Android consumer App', 2012),
    WinningApp('Rapidtargets', 'the best HTML5 App', 2012),
    WinningApp('Matchy', 'the best Windows App', 2012),
    WinningApp('DStv', 'Best IOS Consumer', 2013),
    WinningApp('.comm Telco Data Visualizer', 'Best IOS Enterprise', 2013),
    WinningApp('PriceCheck Mobile', 'the best Blackberry App', 2013),
    WinningApp('MarkitShare', 'the best Android enterprise App', 2013),
    WinningApp('Nedbank App Suite', 'the best Android consumer App', 2013),
    WinningApp('SnapScan', 'the best HTML5 App', 2013),
    WinningApp('Kids Aid', 'the best Windows App', 2013),
    WinningApp('bookly', 'Most innovative app', 2013),
    WinningApp('Gautrain Buddy', 'Best independent (garage) developer', 2013),
    WinningApp('SuperSport', 'Best IOS Consumer', 2014),
    WinningApp('SyncMobile', 'Best IOS Enterprise', 2014),
    WinningApp('MyBelongings', 'Best Android Consumer App', 2014),
    WinningApp('LIVE Inspect', 'the best Android enterprise App', 2014),
    WinningApp('VIGO', 'Best use of Microsoft Cloud Services App', 2014),
    WinningApp('Zapper', 'Best App for Microsoft Platform', 2014),
    WinningApp('Rea Vaya', 'Best Garage Developer App', 2014),
    WinningApp('Wildlife Tracker', 'Most innovative app', 2014),
    WinningApp('Takealot', 'Best Consumer Solution', 2015),
    WinningApp('Standard Bank’s Shyft', 'Best Financial Solution', 2015),
    WinningApp('iiDENTIFii', 'Best Enterprise Solution', 2015),
    WinningApp('SiSa', 'Best Hackathon Solution', 2015),
    WinningApp('Guardian Health', 'Best Health Solution', 2015),
    WinningApp('Murimi', 'Best Agricultural Solution', 2015),
    WinningApp('UniWise', 'Best Campus Cup Solution', 2015),
    WinningApp('Kenyan app, Kazi App', 'Best African Solution', 2015),
    WinningApp(
        'Rekindle Learning',
        'Best Women in STEM (Science, Technology, Engineering and Math) Solution',
        2015),
    WinningApp('Afrihost', 'People’s Choice Award', 2015),
    WinningApp('Hellopay by SoftPOS', 'Most Innovative Solution', 2015),
    WinningApp(
        'Road Save,Hello Paisa, Matric Live', 'Huawei Category 15', 2015),
    WinningApp('Domestly', 'Best Consumer App', 2016),
    WinningApp('iKhoha', 'Best Enterprise App', 2016),
    WinningApp('HearZA', 'Best Enterprise Development App', 2016),
    WinningApp('Tuta-me', 'Best Breakthrough Development App', 2016),
    WinningApp('KaChing', 'Most Innovative App', 2016),
    WinningApp('Friendly Math Monsters for Kindergarten',
        'Best Mobile Gaming App', 2016),
    WinningApp('OrderIN', 'Best Consumer Solution', 2017),
    WinningApp('TransUnion 1Check', 'Best Enterprise Solution', 2017),
    WinningApp('EcoSlips', 'Best Incubated Solution', 2017),
    WinningApp('InterGreatMe', 'Most Innovative Solution', 2017),
    WinningApp('Zulzi', 'Best Breakthrough Developer', 2017),
    WinningApp('Hey Jude', 'Best South African App', 2017),
    WinningApp(
        'Oru Social, TouchSA',
        'Women in Science, Technology, Engineering and Mathematics (STEM) Solution',
        2017),
    WinningApp('Pick n Pay Super Animals 2', 'Best Gaming Solution', 2017),
    WinningApp('The TreeApp South Africa', 'Best Agriculture Solution', 2017),
    WinningApp('WatIf Health Portal', 'Best Health Solution', 2017),
    WinningApp('Awethu Project', 'Best Education Solution', 2017),
    WinningApp('Best Financial Solution', 'Shyft for Standard Bank', 2017),
    WinningApp('Pineapple', 'Best Consumer Solution', 2018),
    WinningApp('Cowa Bunga', 'Best Enterprise Solution', 2018),
    WinningApp('Digemy Knowledge Partner and Besmarter',
        'Best Incubated Solution', 2018),
    WinningApp('Bestee', 'Most Innovative Solution', 2018),
    WinningApp('The African Cyber Gaming League App (ACGL)',
        'Best Gaming Solution', 2018),
    WinningApp('dbTrack', 'Best Health Solution', 2018),
    WinningApp('Bestee', 'Best Breakthrough Developer', 2018),
    WinningApp('Stokfella', 'Best South African Solution', 2018),
    WinningApp('Difela Hymns', 'Women in STEM Solution', 2018),
    WinningApp('Xander English 1-20', 'Best Education Solution', 2018),
    WinningApp('Ctrl', 'Best Financial Solution', 2018),
    WinningApp('Khula', 'Best Agriculture Solution', 2018),
    WinningApp('ASI Snakes', 'People’s Choice Award', 2018),
    WinningApp('Khula', 'App of the Year', 2018),
    WinningApp('SI Realities', 'Most Innovative Solution', 2019),
    WinningApp('Lost Defence', 'Best Gaming App', 2019),
    WinningApp('The African Cyber Gaming League App (ACGL)',
        'Best Gaming Solution', 2019),
    WinningApp('Franc', 'Best South African App', 2019),
    WinningApp('Vula Mobile', 'Best Health Solution', 2019),
    WinningApp('Matric Live', 'Best Educational Solution', 2019),
    WinningApp(
        'My Pregnancy Journal and LocTransie',
        'Best Women in Science Technology Engineering and Mathematics (Stem) Solution',
        2019),
    WinningApp('LocTransie', 'Best Incubated Solution', 2019),
    WinningApp('Hydra', 'Best Agricultural Solution', 2019),
    WinningApp('Bottles', 'Best Consumer App', 2019),
    WinningApp('Over', 'People’s Choice Award', 2019),
    WinningApp('Digger', 'Best Enterprise App', 2019),
    WinningApp('Mo Wash', 'Best Breakthrough Developer', 2019),
    WinningApp('Examsta', 'Best Women in STEM', 2020),
    WinningApp('Checkers Sixty60', 'Best Enterprise Solution', 2020),
    WinningApp('Technishen', 'Best Incubated Solution', 2020),
    WinningApp('BirdPro', 'Most Innovative Solution', 2020),
    WinningApp('Lexie Hearing', 'Best Gaming Solution', 2020),
    WinningApp('GreenFingers Mobile', 'Best Agricultural Solution', 2020),
    WinningApp('Xitsonga Dictionary', 'Best Educational Solution', 2020),
    WinningApp('StokFella', 'Best Financial Solution', 2020),
    WinningApp('Bottles', 'Best South African App', 2020),
    WinningApp('Guardian Health', 'Best Youth App', 2020),
    WinningApp('Checkers Sixty60', 'People’s Choice Award', 2020),
    WinningApp('My Pregnancy Journey', 'Best Huawei Mobile Services App', 2020),
    WinningApp('iiDENTIFii app', 'Best Enterprise Solution', 2021),
    WinningApp('Hellopay SoftPOS', 'Most Innovative Solution', 2021),
    WinningApp('Guardian Health Platform', 'Best Health Solution', 2021),
    WinningApp('Ambani Africa', 'Best Gaming Solution', 2021),
    WinningApp('Murimi', 'Best Agricultural Solution', 2021),
    WinningApp('Ambani Africa', 'Best Educational Solution', 2021),
    WinningApp('Shyft for Standard Bank', 'Best Financial Solution', 2021),
    WinningApp('Sisa', 'Best Hackathon Solution', 2021),
    WinningApp('Ambani Africa', 'Best South African App', 2021),
    WinningApp('Kazi', 'Best African Solution', 2021),
    WinningApp('Takealot app', 'Best Consumer Solution', 2021),
    WinningApp('Rekindle Learning app', 'Best Women in STEM', 2021),
    WinningApp('Roadsave', 'Huawei Category 15', 2021),
    WinningApp('Afrihost', 'People’s Choice Award', 2021),
  ];

  winningApps
      .sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));
  print('\nAll Winning apps from 2012:');
  for (var winningApp in winningApps) {
    print(winningApp.name);
  }

  var winningAppsFor2017And20018 = winningApps.where(
      (winningApp) => winningApp.year == 2017 || winningApp.year == 2018);
  print('\nWinning apps for 2017 and 2018:');
  for (var winningApp in winningAppsFor2017And20018) {
    print(winningApp.name);
  }

  print('\nTotal number of winning apps: ${winningApps.length}');
}
